package src.main.java.com.epam.sorting;

import java.util.Arrays;

public class Task2 {
    public static void main(String[] args) {
        String[] array = {"Cake", "Pie", "Pizza", "Sushi", "Coffee", "Tea", "Latte", "Bagel"};
        String[] sortedArrayBubbleSort = bubbleSort(array);
        String[] sortedArrayInsertionSort = insertionSort(array);
        String[] sortedArraySelectionSort = selectionSort(array);
        printArray(sortedArrayBubbleSort);
        printArray(sortedArrayInsertionSort);
        printArray(sortedArraySelectionSort);
    }

    public static String[] bubbleSort(String[] array) {
        String[] resultArray = Arrays.copyOf(array, array.length);

        for (int i = 0; i < resultArray.length - 1; i++) {
            for (int j = 0; j < resultArray.length - 1 - i; j++) {
                int isGreater = resultArray[j].compareTo(resultArray[j + 1]);
                if (isGreater > 0) {
                    String temp = resultArray[j + 1];
                    resultArray[j + 1] = resultArray[j];
                    resultArray[j] = temp;
                }
            }
        }

        return resultArray;
    }

    public static String[] insertionSort(String[] array) {
        String[] resultArray = Arrays.copyOf(array, array.length);

        for (int i = 1; i < resultArray.length; i++) {
            String current = resultArray[i];
            int previousIndex = i - 1;
            while (previousIndex >= 0 && current.compareTo(resultArray[previousIndex]) < 0) {
                resultArray[previousIndex + 1] = resultArray[previousIndex];
                resultArray[previousIndex] = current;
                previousIndex--;
            }
        }

        return resultArray;
    }

    public static String[] selectionSort(String[] array) {
        String[] resultArray = Arrays.copyOf(array, array.length);

        int positionOfMinimum;
        for (int i = 0; i < resultArray.length; i++) {
            positionOfMinimum = i;
            for (int j = i; j < resultArray.length; j++) {
                int isLess = resultArray[j].compareTo(resultArray[positionOfMinimum]);
                if (isLess < 0) {
                    positionOfMinimum = j;
                }
            }
            String temp = resultArray[positionOfMinimum];
            resultArray[positionOfMinimum] = resultArray[i];
            resultArray[i] = temp;
        }

        return resultArray;
    }

    public static void printArray(String[] array) {
        for (String string : array) {
            System.out.print(string + " ");
        }
        System.out.println();
    }
}
