package src.main.java.com.epam.sorting;

import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) {
        char[] array = {'t', 's', 'a', 'y', 'z', 'e', 'b'};
        char[] sortedArrayBubbleSort = bubbleSort(array);
        char[] sortedArrayInsertionSort = insertionSort(array);
        char[] sortedArraySelectionSort = selectionSort(array);
        printArray(sortedArrayBubbleSort);
        printArray(sortedArrayInsertionSort);
        printArray(sortedArraySelectionSort);
    }

    public static char[] bubbleSort(char[] array) {
        char[] resultArray = Arrays.copyOf(array, array.length);

        for (int i = 0; i < resultArray.length - 1; i++) {
            for (int j = 0; j < resultArray.length - 1 - i; j++) {
                int isGreater = Character.compare(resultArray[j], resultArray[j + 1]);
                if (isGreater > 0) {
                    char temp = resultArray[j + 1];
                    resultArray[j + 1] = resultArray[j];
                    resultArray[j] = temp;
                }
            }
        }

        return resultArray;
    }

    public static char[] insertionSort(char[] array) {
        char[] resultArray = Arrays.copyOf(array, array.length);

        for (int i = 1; i < resultArray.length; i++) {
            char current = resultArray[i];
            int previousIndex = i - 1;
            while (previousIndex >= 0 && Character.compare(current, resultArray[previousIndex]) < 0) {
                resultArray[previousIndex + 1] = resultArray[previousIndex];
                resultArray[previousIndex] = current;
                previousIndex--;
            }
        }

        return resultArray;
    }

    public static char[] selectionSort(char[] array) {
        char[] resultArray = Arrays.copyOf(array, array.length);

        int positionOfMinimum;
        for (int i = 0; i < resultArray.length; i++) {
            positionOfMinimum = i;
            for (int j = i; j < resultArray.length; j++) {
                int isLess = Character.compare(resultArray[j], resultArray[positionOfMinimum]);
                if (isLess < 0) {
                    positionOfMinimum = j;
                }
            }
            char temp = resultArray[positionOfMinimum];
            resultArray[positionOfMinimum] = resultArray[i];
            resultArray[i] = temp;
        }

        return resultArray;
    }

    public static void printArray(char[] array) {
        for (char character : array) {
            System.out.print(character + " ");
        }
        System.out.println();
    }
}
