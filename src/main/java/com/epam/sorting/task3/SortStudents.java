package src.main.java.com.epam.sorting.task3;

public class SortStudents {
    public static void main(String[] args) {
        String[] firstNames = {"Egor", "Artiom", "Egor", "Vlad", "Igor", "Ivan", "Kirill"};
        String[] lastNames = {"Anufriev", "Abibekov", "Kalinchuk", "Ilyin", "Loginov", "Minin", "Fomichev"};

        sortStudents(firstNames, lastNames);
        printStudents(firstNames, lastNames);
    }

    public static void sortStudents(String[] firstNames, String[] lastNames) {
        for (int i = 0; i < firstNames.length - 1; i++) {
            for (int j = 0; j < firstNames.length - 1 - i; j++) {
                int isFirstNameGreater = firstNames[j].compareTo(firstNames[j + 1]);
                int isLastNameGreater = lastNames[j].compareTo(lastNames[j + 1]);
                if (isFirstNameGreater > 0 || (isFirstNameGreater == 0 && isLastNameGreater > 0)) {
                    String tempFirstName = firstNames[j + 1];
                    firstNames[j + 1] = firstNames[j];
                    firstNames[j] = tempFirstName;

                    String tempLastName = lastNames[j + 1];
                    lastNames[j + 1] = lastNames[j];
                    lastNames[j] = tempLastName;
                }
            }
        }
    }

    public static void printStudents(String[] firstNames, String[] lastNames) {
        for (int i = 0; i < firstNames.length; i++) {
            System.out.println(firstNames[i] + " " + lastNames[i]);
        }
    }
}
